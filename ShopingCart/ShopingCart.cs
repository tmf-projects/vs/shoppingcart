﻿using System;
using System.Collections.Generic;
using System.Dynamic;

namespace ShopingCartLibrary
{
    [Serializable]
    public class ShopingCart : IDisposable
    {
        public List<Item> Items = new List<Item>();

        public int Count
        {
            get { return Items.Count; }
        }

        public void Add(Item item)
        {
            Items.Add(item);
        }

        public void Dispose()
        {
            // удалять
        }

        public void Remove(int index)
        {
            Items.RemoveAt(index);
        }
    }

    [Serializable]
    public class Item
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
