﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopingCartLibrary;
using System.Diagnostics;
using System;

namespace ShopingCartLibrary.Tests
{
    [TestClass]
    public class TestInitAndCleanup
    {
        private ShopingCart cart;
        private Item item;

        // запуск перед каждым тест методом
        [TestInitialize]
        public void TestInitialize()
        {
            Debug.WriteLine("Test Initialize");
            item = new Item();
            item.Name = "Usit Test Project Practic Work";
            item.Quantity = 10;

            cart = new ShopingCart();
            cart.Add(item);
        }

        // запуск после каждого тест метода
        [TestCleanup]
        public void TestCleanUp()
        {
            Debug.WriteLine("Test CleanUp");
            cart.Dispose();
        }

        [TestMethod]
        public void ShopingCart_CheckOut_ContainsItem()
        {
            CollectionAssert.Contains(cart.Items, item);
        }

        [TestMethod]
        public void ShopingCart_RemoveItem_Empty()
        {
            int expected = 0;

            cart.Remove(0);

            Assert.AreEqual(expected, cart.Count);
        }
    }
}
